import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Players from './players/Players';

const Routes = () => (
  <Router>
        <Route exact path="/" component={Players} />
  </Router>
);

export default Routes;

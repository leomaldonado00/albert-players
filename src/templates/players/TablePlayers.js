import React from 'react';
import styled from 'styled-components';
import moment from 'moment';
import Diver from '../../components/Diver';

const TablePlayers = props => {
  const {players}=props;

  return (
    <div>
                <Diver marginTop="25px" marginBottom="25px">
                    <WrapList>
                    <Table>
                        <Thead>
                            <Tr>
                                <Td>Player</Td>
                                <Td>Position</Td>
                                <Td>Nationality</Td>
                                <Td>Age</Td>
                            </Tr>
                        </Thead>

                        <tbody>
                        {players ? 
                        players.map((element, i) => (
                            <Tr key={i} backgroundColor={i%2===0?"silver":"none"}>
                                <Td>{element.name}</Td>
                                <Td>{element.position}</Td>
                                <Td>{element.nationality}</Td>
                                <Td>{moment(new Date()).diff(moment(element.dateOfBirth, 'YYYY-MM-DD'), 'years').toString()}</Td>
                            </Tr>
                            ))
                            : null}    
                                                
                        </tbody>
                    </Table>
                    </WrapList>
                </Diver>
    </div>
  );
};

const Table = styled.table`
border-spacing: 0;
border: 1px solid #000;
width: 100%;
max-width: 1200px;
min-width: 800px;
margin-left: auto;
margin-right: auto;

`;
const Tr = styled.tr`
border-spacing: 0;
background-color: ${props => props.backgroundColor || "none" };
`;
const Td = styled.td`
vertical-align: center;
text-align: center;
border: 1px solid #000;
border-spacing: 0;
height:50px;
min-width: 200px;
`;
const Thead = styled.thead`
background-color:gray;
color:white;
`;
const WrapList = styled.div`
  overflow-x: auto;
`;

export default TablePlayers;

import React, { useEffect, useState } from "react";
import { useDispatch, connect } from "react-redux";
import styled from 'styled-components';
import {
  searchPlayers,
  filterPlayers,
} from "../../ducks/players";
import { getPlayer, getLoading, getError, getPosition } from "../../ducks/players/selector";
import LayoutPlayers from '../../components/LayoutPlayers';
import Text from '../../components/Text';
import Diver from '../../components/Diver';
import Button from '../../components/Button';
import TablePlayers from "./TablePlayers";

const Players = ({ players, position, loading, error }) => {

  const dispatch = useDispatch();
  const [formLogin, setFormLogin] = useState({
    name: ``,
    position: ``,
    age: ``,
    Ename: "",
    Eposition: "",
    Eage: "",
    Egral: "",
  });

  useEffect(() => {
    dispatch(searchPlayers());
    dispatch(filterPlayers({ name:"", position:"", age:"" }));
  }, [dispatch]);

  const validate = (dataLogin) => {
    let errores = {};
    if (!dataLogin.name && !dataLogin.position && !dataLogin.age) {
      errores = {
        ...errores,
        Egral: "You must fill at least one field",
        Eflag: true,
      };
      setFormLogin({ ...dataLogin, ...errores });
      return false;
    }
    if (
      dataLogin.name.length !== 0 &&
      !/^(([A-Za-zñáÁéÉíÍóÓúÚç]+[\s]*)+){1,50}$/.test(dataLogin.name)
    ) {
      errores = {
        ...errores,
        Ename: "Name must have alphabetic characters.",
        Eflag: true,
      };
      setFormLogin({ ...dataLogin, ...errores });
      return false;
    }
    if (
        dataLogin.age.length !== 0 &&
        !/^[\d+]{1,2}$/.test(dataLogin.age) //   ^[\d/+]{1,17}$
      ) {
        errores = {
          ...errores,
          Eage:
            'The age must be only numbers.',
          Eflag: true,
        };
        setFormLogin({ ...dataLogin, ...errores });
        return false;
      }  
    if (
      dataLogin.age.length !== 0 &&
      (parseInt(dataLogin.age) < 18 || parseInt(dataLogin.age) > 40)
    ) {
      errores = {
        ...errores,
        Eage: "The age must be between 18 and 40.",
        Eflag: true,
      };
      setFormLogin({ ...dataLogin, ...errores });
      return false;
    }

    setFormLogin({ ...dataLogin, Eflag: false });
    return true;
  };

  const validate0 = (dataLogin) => {
    const valid = validate(dataLogin);
    const { name, position, age } = dataLogin;
    const dataLoginF = { name:name.trim(), position:position.trim(), age:age.trim() };
    if (valid) {
      dispatch(filterPlayers(dataLoginF));
    } 
  };
  const clear = () => {
    setFormLogin({
      ...formLogin,
      name: ``,
      position: ``,
      age: ``,
      Ename: "",
      Eposition: "",
      Eage: "",
      Egral: "",
    });
    dispatch(filterPlayers({ name:"", position:"", age:"" }));
  };

  const escrib = (e) => {
    const regx = / +/g;
    if (!/^\s/.test(e.target.value)) {
      if (e.target.name === "name") {
        if (e.target.value.length <= 60) {
          setFormLogin({
            ...formLogin,
            [e.target.name]: e.target.value.replace(regx, " "),
            [`E${e.target.name}`]: "",
            Egral: "",
          });
        }
      }
      if (e.target.name === "age") {
        if (e.target.value.length <= 2) {
          setFormLogin({
            ...formLogin,
            [e.target.name]: e.target.value.replace(regx, " "),
            [`E${e.target.name}`]: "",
            Egral: "",
          });
        }
      }
      if (e.target.name === "position") {
        if (e.target.value !== 'no') {
            setFormLogin({
          ...formLogin,
          [e.target.name]: e.target.value.replace(regx, " "),
          [`E${e.target.name}`]: "",
          Egral: "",
        });
    }
      }
    }
  };

  return (
    <div>
        <LayoutPlayers>

            <Diver
            display="flex"
            flexDir="column"
            alignItems="center"
            width="100%"
            height="auto"
            marginTop="10px"
        >
            <Diver maxWidth="1200px" width="90%" width62="95%" marginTop="25px">
                <Text fontSize="25px" fontSize62="24px" fontWeight="600" color="#262626" textAlign="left">Football Player Finder</Text>
                
                {formLogin.Egral.length !== 0 ? <Error marginTop="25px">{formLogin.Egral}</Error> : null}

                <Diver display="flex" justifyContent="space-between" flexDir92="column">


                    <Diver  maxWidth="1200px" width62="95%" marginTop="25px" marginLeft="auto" marginRight="auto">


                        <Input
                            type="text"
                            name="name"
                            placeholder="Player Name"
                            onChange={(e) => escrib(e)}
                            value={formLogin.name}
                            error={formLogin.Ename}
                        />
                        {formLogin.Ename.length !== 0 ? <Error>{formLogin.Ename}</Error> : null}
                    </Diver>

                    <Diver  maxWidth="1200px" width62="95%" marginTop="25px" marginLeft="auto" marginRight="auto">
                        <Select
                            name="position"
                            onChange={(e) => escrib(e)}
                            error={formLogin.Eposition}
                            value={formLogin.position}
                        >
                            <Option value="">Position</Option>

                            {position ? 
                            position.map((element, i) => (
                            <Option key={i} value={element}>{element}</Option>
                            ))
                            :
                            <Option value="no">Results were not loaded</Option>
                            }
                        </Select>
                    </Diver>
                    <Diver  maxWidth="1200px"  width62="95%" marginTop="25px" marginLeft="auto" marginRight="auto">
                        <Input
                            type="text"
                            name="age"
                            placeholder="Age"
                            onChange={(e) => escrib(e)}
                            value={formLogin.age}
                            error={formLogin.Eage}
                        />
                        {formLogin.Eage.length !== 0 ? <Error>{formLogin.Eage}</Error> : null}
                    </Diver>
                    <Diver  maxWidth="1200px" width62="95%" marginTop="25px" marginLeft="auto" marginRight="auto">
                    <Button
                        text="Search"
                        fontSize="18px"
                        fontWeight="300"
                        width="100px"
                        widthTele="90%"
                        background="#262626"
                        color="white"
                        backgroundColorH="#262626"
                        colorH="white"
                        border="1px solid #262626"
                        borderRadius="10px"
                        height="36px"
                        height38="30px"
                        margin="0px"
                        marginDiv="auto"
                        onClick={() => validate0(formLogin)}
                    />
                    </Diver>

                    <Diver  maxWidth="1200px" width62="95%" marginTop="25px" marginLeft="auto" marginRight="auto">
                    <Button
                        text="Clear"
                        fontSize="18px"
                        fontWeight="300"
                        width="100px"
                        widthTele="90%"
                        background="white"
                        color="#262626"
                        backgroundColorH="white"
                        colorH="#262626"
                        border="1px solid #262626"
                        borderRadius="10px"
                        height="36px"
                        height38="30px"
                        margin="auto"
                        marginDiv="auto"
                        onClick={() => clear()}
                    />
                    </Diver>

                </Diver>

                <TablePlayers players={players} loading={loading} />
                {loading ? <div> LOADING..................................</div> : null}
                {players && players.length === 0 ? <div>There are no matching results</div> :null}
                {error ? <Error>{error}</Error> : null}


            </Diver>

        
        </Diver>

      </LayoutPlayers>
    </div>
  );
};

const Input = styled.input`
  width: 180px;
  height: 30px;
  border-radius: 5px;
  background-color: #f6f6f6;
  border: 1px solid #c8c8d0;
  margin: 0;
  padding-left: 5px;
  outline: none;
  color: gray;
  @media (max-width: 480px) {
    width:calc(100% - 7px);
  }
`;
const Select = styled.select`
  width: 191px;
  height: 34px;
  border-radius: 5px;
  background-color: #f6f6f6;
  border: 1px solid #c8c8d0;
  margin: 0;
  padding-left: 4px;
  outline: none;
  color: gray;
  @media (max-width: 480px) {
    width:calc(100% - 0px);
  }
`;
const Option = styled.option`
  color: gray;
`;
const Error = styled.div`
  font-family: Montserrat;
  color: red;
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.29;
  text-align: center;
  margin-top:${props => props.marginTop || "3px" };
  margin-bottom: ${props => props.marginBottom || "0px" };
`;

function mapStateToProps(state) {
  return {
    players: getPlayer(state),
    position: getPosition(state),
    loading: getLoading(state),
    error: getError(state),
  };
}

export default connect(mapStateToProps)(Players);

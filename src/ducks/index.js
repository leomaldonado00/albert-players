import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage'; 
import createSagaMiddleware from 'redux-saga'; 
import playersStore from './players';

const reducers = combineReducers({
  playersStore,
});

const persistConfig = {
  key: 'SearchPlayers',
  storage,
};
const persistedReducer = persistReducer(persistConfig, reducers);
const sagaMiddleware = createSagaMiddleware();

export default () => {
  const store = createStore(
    persistedReducer,
    undefined,
    compose(applyMiddleware(sagaMiddleware)), 
  );
  const persistor = persistStore(store);
  return { store, persistor, sagaMiddleware };
};

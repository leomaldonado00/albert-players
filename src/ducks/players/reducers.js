import * as types from './types';

const InitialState = {};

export default function playersStore(state = InitialState, action) {
  switch (action.type) {
    case types.FILTER_PLAYERS_START:
      return {
        ...state,
        age: action.payload.age,
        name: action.payload.name,
        position: action.payload.position,
      };
    case types.SEARCH_PLAYERS_START:
      return {
        ...state,
        showModal: false,
        msgModal: null,
        loading: true,
        players: null,
        error: null,
        errorL: false,
      };
    case types.SEARCH_PLAYERS_COMPLETE:
      return {
        ...state,
        msgModal: null,
        loading: null,
        players: action.players.data,
        error: null,
        showModal: false,
        errorL: false,
      };
    case types.SEARCH_PLAYERS_ERROR:
      return {
        ...state,
        loading: null,
        showModal: true,
        error: action.error,
        players: null,
        msgModal: `Error al searchPlayers`,
        errorL: true,
      };

    default:
      return state;
  }
}

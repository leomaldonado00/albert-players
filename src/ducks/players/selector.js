import { createSelector } from 'reselect'
import moment from 'moment';

const getPlayers = (state) => {
    return state.playersStore.players
}
const getLoadings = (state) => {
    return state.playersStore.loading
}
const getErrors = (state) => {
    return state.playersStore.error
}
const getNames = (state) => {
    return state.playersStore.name
}
const getPositionsToFilter = (state) => {
    return state.playersStore.position
}
const getAges = (state) => {
    return state.playersStore.age
}

export const getPlayer = createSelector([getPlayers,getNames,getPositionsToFilter,getAges], (getPlayers,getNames,getPositionsToFilter,getAges) => {
    if(getPlayers) {
        let playersToShow = [];
            if(getNames || getPositionsToFilter || getAges){
            for (let i = 0; i < getPlayers.length; i++) {

                let nameF = true;
                let positionF = true;
                let ageF = true;
                if(getNames.length!==0){
                    if (getPlayers[i].name.toLowerCase().indexOf(getNames.toLowerCase()) > -1) {
                        nameF=true;
                    }else{
                        nameF=false;
                    }  
                }    
                if(getPositionsToFilter.length!==0){
                    if (getPlayers[i].position===getPositionsToFilter) {
                        positionF=true;
                    }else{
                        positionF=false;
                    }  
                }
                if(getAges.length!==0){
                    const fD = moment(new Date());
                    const sD = moment(getPlayers[i].dateOfBirth, 'YYYY-MM-DD');
                    const age = fD.diff(sD, 'years').toString();
                    if (age===getAges) {
                        ageF=true;
                    }else{
                        ageF=false;
                    }  
                }
                if(nameF&&positionF&&ageF){
                    playersToShow= [...playersToShow, getPlayers[i]]
                }
        
        }

            return playersToShow;
        }

        return getPlayers;
        
    }
});
export const getPosition = createSelector([getPlayers], (getPlayers) => {
    if(getPlayers) {
        const allPositions = getPlayers.map((i) => i.position);
        let positions = [];
        for (let i = 0; i < allPositions.length; i++) {
            const here = positions.indexOf(allPositions[i]); 
            if (here === -1) {
                positions = [...positions, allPositions[i]];            }
          }
        return positions;
    }
});
export const getLoading = createSelector([getLoadings], (getLoadings) => {
        return getLoadings;
});
export const getError = createSelector([getErrors], (getErrors) => {
        return getErrors;
});

import { put, call, takeLatest } from 'redux-saga/effects';
import * as types from './types';
import { apiCall } from '../../api';

export function* searchPlayers({ payload }) {
  try {
    const players = yield call(apiCall, null, null, null , 'GET');
    if (players !== undefined) { //  && players.status === 200
      yield put({
        type: types.SEARCH_PLAYERS_COMPLETE,
        players,
      });
    }
  } catch (error) {
    if (error.response !== undefined) {
      if (error.response.status === 401) {
        const errorDirec = 'Requires authorization for this area';
        yield put({
          type: types.SEARCH_PLAYERS_ERROR,
          error: errorDirec,
        });
      } else if (error.response.status === 404) {
        const errorDirec = 'This address does not exist';
        yield put({
          type: types.SEARCH_PLAYERS_ERROR,
          error: errorDirec,
        });
      } else if (error.response.status === 500) {
        const errorServer =
          'Sorry. There has been an internal server problem';
        yield put({
          type: types.SEARCH_PLAYERS_ERROR,
          error: errorServer,
        });
      } else {
        yield put({
          type: types.SEARCH_PLAYERS_ERROR,
          error:
            error.response.data.non_field_errors || error.response.data.detail,
        });
      }
    } else {
      const errorConex = 'CONNECTION ERROR';
      yield put({
        type: types.SEARCH_PLAYERS_ERROR,
        error: errorConex,
      });
    }
  }
}

export function* sagaPlayers() {
  yield takeLatest(types.SEARCH_PLAYERS_START, searchPlayers);
}


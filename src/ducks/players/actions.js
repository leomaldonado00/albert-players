import * as types from './types';

const searchPlayers = payload => ({
  type: types.SEARCH_PLAYERS_START,
  payload,
});
const filterPlayers = payload => ({
  type: types.FILTER_PLAYERS_START,
  payload,
});


export {
  searchPlayers, filterPlayers
};

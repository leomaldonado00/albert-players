import { all } from 'redux-saga/effects';
import { sagaPlayers } from './players/sagas';


export default function* rootSaga() {
  yield all([sagaPlayers()]);
};

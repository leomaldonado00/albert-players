import React from 'react';
import styled from 'styled-components';
import Header from './Header';

const LayoutLanding = props => (
  <div>
    <Header itemHeader={props.itemHeader} history={props.history} goToId={props.goToId} arrow={props.arrow} />
    <Children>{props.children}</Children>
  </div>
);

const Children = styled.div`
  width: 100%;
  min-height: calc(100vh - 320px);
  margin-top: 64px;
  /* margin-bottom: 20vh; */
`;
export default LayoutLanding;
import React from 'react';
import styled from 'styled-components';
// import { Link } from "react-router-dom";

const Button = props => {
  return (
    <ButtonDiv 
    onClick={props.onClick}
    width={props.width}
    width38={props.width38}
    marginDiv={props.marginDiv}
    >
      <ButtonB
          background={props.background}
          color={props.color}
          backgroundColorH={props.backgroundColorH}
          colorH={props.colorH}
          border={props.border}
          borderRadius={props.borderRadius}
          height={props.height}
          height38={props.height38}
          margin={props.margin}
          fontFamily={props.fontFamily}
          fontSize={props.fontSize}
          fontWeight={props.fontWeight}
      >
        {props.text}
      </ButtonB>
    </ButtonDiv>
  );
};

const ButtonDiv = styled.div`
  height: auto;
  /*margin-top:20px;*/
  width:  ${props => props.width || "330px" };
  margin:  ${props => props.marginDiv || "0px" };
  cursor: pointer;
  @media (max-width: 380px) {
    width:  ${props => props.width38 || "90%" };
  }
`;

const ButtonB = styled.button`
  margin:  ${props => props.margin || "20px auto 0px auto" };
  border-radius:  ${props => props.borderRadius || "40px" };
  color:  ${props => props.color || "#00beb6" };
  background:  ${props => props.background || "transparent" };
  opacity: 1;
  border:  ${props => props.border || "1px solid #00beb6" };
  font-size: ${props => props.fontSize || "15px" }; 
  font-family: ${props => props.fontFamily || "Source Sans Pro" };
  width: 100%;
  height:  ${props => props.height || "35px" };
  outline: none;
  font-weight: ${props => props.fontWeight || "600" };
  padding: 5px 9px;
  text-align: center;
  text-transform: inherit;
  text-overflow: ellipsis;
  cursor: pointer;
  transition-duration: 0.4s;
  transition-timing-function: ease-out;
  @media (max-width: 380px) {
    font-size: 13px;
    height:  ${props => props.height38 || "32px" };
  }
  :hover {
    background-color::  ${props => props.backgroundColorH || "#00beb6" };
    color:  ${props => props.colorH || "#fff" };
  }
`;

export default Button;

import React from 'react';
import styled from 'styled-components';
import { IoMdFootball } from 'react-icons/io';
import Text from './Text';

const Header = props => {
  
  return (
    <div>
        <HeaderDivMov>

            <Toggle>
              <IoMdFootball size={20} color="white" />
              <IoMdFootball size={20} color="white" />
              <IoMdFootball size={20} color="white" />
            </Toggle>

            <WrapArrowLogo>

          {/* props.arrow ? <ArrowBack history={props.history} /> : null */}
          <Text fontSize="28px" fontSize62="24px" fontWeight="600" color="white">Fanatic</Text>
          </WrapArrowLogo>

        </HeaderDivMov>

    </div>
  );
};

const WrapArrowLogo = styled.div`
  display: flex;
  margin:auto; /* center slf */
`;
const HeaderDivMov = styled.div`
display:flex;
  position: fixed;
  top: 0px;
  z-index: 999999;
  width: 100%;
  background: #262626;
  height: 64px;
  align-items: center;
`;

const Toggle = styled.div`
  position:absolute;  /* left slf */
  width: auto;
  height: 40px;
  display: flex;
  align-items: center;
  margin-left: 80px;
  @media (max-width: 620px) {
    margin-left: 10px;
  }
`;

export default Header;

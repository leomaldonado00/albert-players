import React from 'react';
import styled from 'styled-components';

const Text = props => (
  <WrapText
    title={props.title}
    color={props.color}
    colorh={props.colorh}
    textAlign={props.textAlign}
    marginTop={props.marginTop}
    marginBottom={props.marginBottom}
    marginRight={props.marginRight}
    marginLeft={props.marginLeft}
    fontFamily={props.fontFamily}
    fontStretch={props.fontStretch}
    fontWeight={props.fontWeight}
    fontSize={props.fontSize}
    fontStyle={props.fontStyle}
    lineHeight={props.lineHeight}
    display={props.display}
    letterSpacing={props.letterSpacing}
    opacity={props.opacity}
    cursor={props.cursor}
    width={props.width}
    maxWidth={props.maxWidth}
    paddingTop={props.paddingTop}
    paddingRight={props.paddingRight}
    paddingBottom={props.paddingBottom}
    paddingLeft={props.paddingLeft}
    fontSize10={props.fontSize10}
    fontSize99={props.fontSize99}
    title92={props.title92}
    color92={props.color92}
    textAlign92={props.textAlign92}
    marginTop92={props.marginTop92}
    marginBottom92={props.marginBottom92}
    marginRight92={props.marginRight92}
    marginLeft92={props.marginLeft92}
    fontFamily92={props.fontFamily92}
    fontStretch92={props.fontStretch92}
    fontWeight92={props.fontWeight92}
    fontSize92={props.fontSize92}
    fontStyle92={props.fontStyle92}
    lineHeight92={props.lineHeight92}
    display92={props.display92}
    letterSpacing92={props.letterSpacing92}
    opacity92={props.opacity92}
    cursor92={props.cursor92}
    width92={props.width92}
    maxWidth92={props.maxWidth92}
    paddingTop92={props.paddingTop92}
    paddingRight92={props.paddingRight92}
    paddingBottom92={props.paddingBottom92}
    paddingLeft92={props.paddingLeft92}
    fontSize62={props.fontSize62}
    marginTop48={props.marginTop48}
    marginBottom48={props.marginBottom48}
    marginLeft48={props.marginLeft48}
    marginRight48={props.marginRight48}
    width48={props.width48}
    position={props.position}
    overflowX={props.overflowX}
    overflowY={props.overflowY}
    textOF={props.textOF}
    aniName={props.aniName}
    aniDura={props.aniDura}
    aniIterCount={props.aniIterCount}  
    aniTimFun={props.aniTimFun} 
    keyFrames={props.keyFrames}  
    onClick={props.onClick}     
  >
    {props.text || props.children}
  </WrapText>
);
const WrapText = styled.div`
  text-align: ${props => props.textAlign || 'center'};
  margin-top: ${props => props.marginTop || '0'};
  margin-right: ${props => props.marginRight || '0'};
  margin-bottom: ${props => props.marginBottom || '0'};
  margin-left: ${props => props.marginLeft || '0'};
  color: ${props => props.color || '#4a5763'};
  font-weight: ${props => props.fontWeight || 'normal'};
  font-family: ${props => props.fontFamily || 'Source Sans Pro'};
  font-size: ${props => props.fontSize || '13px'};
  font-stretch: ${props => props.fontStretch || 'normal'};
  font-style: ${props => props.fontStyle || 'normal'};
  line-height: ${props => props.lineHeight || '1.29'};
  display: ${props => props.display || 'block'};
  letter-spacing: ${props => props.letterSpacing || 'normal'};
  opacity: ${props => props.opacity || '1'};
  cursor: ${props => props.cursor || 'auto'};
  width: ${props => props.width || 'auto'};
  max-width: ${props => props.maxWidth || 'none'};
  padding-top: ${props => props.paddingTop || '0'};
  padding-right: ${props => props.paddingRight || '0'};
  padding-bottom: ${props => props.paddingBottom || '0'};
  padding-left: ${props => props.paddingLeft || '0'};
  position: ${props => props.position || 'static'};
  overflow-x: ${props => props.overflowX || 'visible'};
  overflow-y: ${props => props.overflowY || 'visible'};
  text-overflow: ${props => props.textOF || 'clip'};
  animation-name: ${props => props.aniName || 'none'};
  animation-duration: ${props => props.aniDura || '0s'};
  animation-iteration-count: ${props => props.aniIterCount || '1'};
  animation-timing-function: ${props => props.aniTimFun || 'ease'};
  @keyframes ${props => props.aniName} {${props => props.keyFrames}}

  &:hover {
    color: ${props => props.colorh || props.color || '#4a5763'};
  }


  @media (max-width: 1000px) {
    font-size: ${props => props.fontSize10 ||   props.fontSize12 || props.fontSize || '13px'};
  }
  @media (max-width: 992px) {
    font-size: ${props =>props.fontSize99 ||  props.fontSize10 ||   props.fontSize12 || props.fontSize || '13px'};
  }
  @media (max-width: 920px) {
    text-align: ${props =>props.textAlign92 || props.textAlign99 || props.textAlign10 ||  props.textAlign12 || props.textAlign || 'center'};
    max-width: ${props =>props.maxWidth92 || props.maxWidth99 || props.maxWidth10 ||  props.maxWidth12 || props.maxWidth || 'none'};
    margin-top: ${props =>props.marginTop92 || props.marginTop99 ||  props.marginTop10 ||   props.marginTop12 || props.marginTop || '0'};
    font-size: ${props => props.fontSize92 || props.fontSize99 ||  props.fontSize10 ||   props.fontSize12 || props.fontSize || '13px'};
  }
  @media (max-width: 620px) {
    font-size: ${props => props.fontSize62 || props.fontSize72 || props.fontSize76 || props.fontSize92 || props.fontSize99 ||  props.fontSize10 ||   props.fontSize12 || props.fontSize || '13px'};
  }
  @media (max-width: 480px) {
    margin-bottom: ${props =>props.marginBottom48 || props.marginBottom62 || props.marginBottom72 || props.marginBottom76 || props.marginBottom92 || props.marginBottom99 ||  props.marginBottom10 ||   props.marginBottom12 || props.marginBottom || '0'};
    margin-top: ${props =>props.marginTop48 || props.marginTop62 || props.marginTop72 || props.marginTop76 || props.marginTop92 || props.marginTop99 ||  props.marginTop10 ||   props.marginTop12 || props.marginTop || '0'};
    margin-left: ${props =>props.marginLeft48 || props.marginLeft62 || props.marginLeft72 || props.marginLeft76 || props.marginLeft92 || props.marginLeft99 ||  props.marginLeft10 ||   props.marginLeft12 || props.marginLeft || '0'};
    margin-right: ${props =>props.marginRight48 || props.marginRight62 || props.marginRight72 || props.marginRight76 || props.marginRight92 || props.marginRight99 ||  props.marginRight10 ||   props.marginRight12 || props.marginRight || '0'};
    width: ${props =>props.width48 || props.width62 || props.width76 || props.width92 || props.width99 ||  props.width10 ||   props.width12 || props.width ||'auto'};
  }


`;

export default Text;

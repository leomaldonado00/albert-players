import axios from 'axios';

const BASE_URL = 'https://football-players-b31f2.firebaseio.com/players.json?print=pretty';

const apiCall = (url, data, headers, method) =>
  axios({
    method,
    url: BASE_URL + url,
    data,
    headers,
    timeout: 3000000000000000000,
  });

export { apiCall };
